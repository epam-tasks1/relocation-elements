﻿using System;

namespace RelocationElementsTask
{
    public static class ArrayExtension
    {
        public static void MoveToTail(int[] source, int value)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (source.Length == 0)
            {
                throw new ArgumentException("Source is empty.");
            }

            int iterationCount = 0;

            for (int i = 0; i < source.Length; i++)
            {
                if (source[i] == value)
                {
                    iterationCount++;
                }
            }

            while (iterationCount != 0)
            {
                for (int i = 0; i < source.Length - 1; i++)
                {
                    if (source[i] == value)
                    {
                        int tmp = source[i];
                        source[i] = source[i + 1];
                        source[i + 1] = tmp;
                    }
                }

                iterationCount--;
            }
        }
    }
}
